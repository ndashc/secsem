﻿#include <iostream>
#include <string>
#include <fstream>
using namespace std;

struct pen
{
    int number;

    string type;
    string brand;
    string color;
};
struct MyQueue
{
    struct Node
    {
        char* data;
        int nnn;
        Node* next;
    };
    Node* First = NULL;
    int Count = 0;
    bool Push(char*, int n);
    bool Pop(char*&, int& n);
};
bool MyQueue::Push(char* data, int n)
{

    if (!First)
    {
        First = new Node;
        First->next = NULL;
        First->data = new char[n];
        for (int i = 0; i < n; i++) (First->data)[i] = data[i];
        Count = 1;
        First->nnn = n;
    }
    else
    {
        Node* temp;
        temp = First;
        while (temp->next != NULL) temp = temp->next;
        temp->next = new Node;
        temp->next->data = new char[n];
        for (int i = 0; i < n; i++) (temp->next->data)[i] = data[i];
        temp->next->nnn = n;
        temp->next->next = NULL;
        Count++;
    }
    return true;
}
bool MyQueue::Pop(char*& data, int& n)
{
    if (!First) return false;
    Node* temp = First->next;
    n = First->nnn;
    data = new char[n];
    for (int i = 0; i < First->nnn; i++) data[i] = (First->data)[i];
    delete[](First->data);
    delete First;
    First = temp;
    Count--;
    return true;
}
void GetSeria(char*& data, int& n, pen A)
{
    size_t s1 = A.type.size();
    size_t s2 = A.brand.size();
    size_t s3 = A.color.size();

    int n1 = sizeof(int);
    int n2_size = sizeof(size_t);
    int n2 = s1;
    int n3_size = sizeof(size_t);
    int n3 = s2;
    int n4_size = sizeof(size_t);
    int n4 = s3;

    n = n1 + n2_size + n2 + n3_size + n3 + n4_size + n4;
    data = new char[n];

    char* d1 = reinterpret_cast<char*> (&A.number);
    char* d2_size = reinterpret_cast<char*> (&s1);
    char* d2 = const_cast<char*> (A.type.c_str());
    char* d3_size = reinterpret_cast<char*> (&s2);
    char* d3 = const_cast<char*> (A.brand.c_str());
    char* d4_size = reinterpret_cast<char*> (&s3);
    char* d4 = const_cast<char*> (A.color.c_str());

    for (int i = 0; i < n1; i++) data[i] = d1[i];
    for (int i = 0; i < n2_size; i++) data[i + n1] = d2_size[i];
    for (int i = 0; i < n2; i++) data[i + n1 + n2_size] = d2[i];
    for (int i = 0; i < n3_size; i++) data[i + n1 + n2_size + n2] = d3_size[i];
    for (int i = 0; i < n3; i++) data[i + n1 + n2_size + n2 + n3_size] = d3[i];
    for (int i = 0; i < n4_size; i++) data[i + n1 + n2_size + n2 + n3_size + n3] = d4_size[i];
    for (int i = 0; i < n4; i++) data[i + n1 + n2_size + n2 + n3_size + n3 + n4_size] = d4[i];
}
void GetDeSeria(char* data, int n, pen& A)
{
    int n1, n2_size, n2, n3_size, n3, n4_size, n4;

    n1 = sizeof(int);

    n2_size = sizeof(size_t);
    size_t p1 = *reinterpret_cast<size_t*> (data + n1);
    n2 = p1;
    string s1(data + n1 + n2_size, p1);

    n3_size = sizeof(size_t);
    size_t p2 = *reinterpret_cast<size_t*> (data + n1 + n2_size + n2);
    n3 = p2;
    string s2(data + n1 + n2_size + n2 + n3_size, p2);

    n4_size = sizeof(size_t);
    size_t p3 = *reinterpret_cast<size_t*> (data + n1 + n2_size + n2 + n3_size + n3);
    n4 = p3;
    string s3(data + n1 + n2_size + n2 + n3_size + n3 + n4_size, p3);

    A.number = *reinterpret_cast<int*> (data);
    A.type = s1;
    A.brand = s2;
    A.color = s3;
}
bool InputBinaryFile(MyQueue& Q)
{
    fstream f_in("data.dat", ios::in | ios::binary);
    if (!f_in)
    {
        return false;
    }
    pen A;
    int n = 0;
    while (!f_in.eof())
    {
        if (f_in.read((char*)&n, sizeof(int)))
        {
            char* data = new char[n];
            f_in.read(data, n);
            GetDeSeria(data, n, A);
            delete[] data;
            GetSeria(data, n, A);
            Q.Push(data, n);
        }
    }
    f_in.close();
    return true;
}
int main()
{
    MyQueue Q;
    char* data;
    int n = 0;
    int m = 1;
    pen A;
    InputBinaryFile(Q);
    while (m)
    {
        cout << "1. Add an item to the queue" << endl;
        cout << "2. Pull out an item" << endl;
        cout << "3. Clear the queue" << endl;
        cout << "0. Exit" << endl;
        cin >> m;
        switch (m)
        {
        case 1:
        {
            system("cls");
            cout << "Enter product specifications:" << endl;
            cout << "Number of pens per box = "; cin >> A.number;
            cout << "Type of pen = "; cin >> A.type;
            cout << "Pen brand = "; cin >> A.brand;
            cout << "Ink color = "; cin >> A.color;

            GetSeria(data, n, A);
            Q.Push(data, n);
            system("cls");

            break;
        }
        case 2:
        {
            system("cls");
            pen A_x;
            bool metka = false;

            cout << "Enter product specifications:" << endl;
            cout << "Number of pens per box = "; cin >> A_x.number;
            cout << "Type of pen = "; cin >> A_x.type;
            cout << "Pen brand = "; cin >> A_x.brand;
            cout << "Ink color = "; cin >> A_x.color;

            int Y = Q.Count;
            for (int i = 0; i < Y; i++)
            {
                Q.Pop(data, n);
                GetDeSeria(data, n, A);
                delete[] data;
                if (A.number != A_x.number || A.type != A_x.type || A.brand != A_x.brand || A.color != A_x.color)
                {
                    GetSeria(data, n, A);
                    Q.Push(data, n);
                    delete[] data;
                }
                else
                {
                    metka = true;
                    break;
                }
            }
            if (!metka) cout << "Product not found" << endl;
            system("pause");
            system("cls");

            break;
        }
        case 3:
        {
            while (Q.Count)
            {
                Q.Pop(data, n);
                GetDeSeria(data, n, A);
                delete[] data;

                cout << "Was removed:" << endl;
                cout << "Number of pens per box = " << A.number << endl;
                cout << "Type of pen = " << A.type << endl;
                cout << "Pen brand = " << A.brand << endl;
                cout << "Ink color = " << A.color << endl << endl;
            }
            system("pause");
            system("cls");

            break;
        }
        }
    }
    while (Q.Count)
    {
        Q.Pop(data, n);
        delete[] data;
    }
}