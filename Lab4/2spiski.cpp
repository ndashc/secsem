#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

struct pen
{
	int number;

	string type;
	string brand;
	string color;
};

void GetSeria(char*& data, int& n, pen A)
{
	size_t s1 = A.type.size();
	size_t s2 = A.brand.size();
	size_t s3 = A.color.size();

	int n1 = sizeof(int);
	int n2_size = sizeof(size_t);
	int n2 = s1;
	int n3_size = sizeof(size_t);
	int n3 = s2;
	int n4_size = sizeof(size_t);
	int n4 = s3;

	n = n1 + n2_size + n2 + n3_size + n3 + n4_size + n4;
	data = new char[n];

	char* d1 = reinterpret_cast<char*> (&A.number);
	char* d2_size = reinterpret_cast<char*> (&s1);
	char* d2 = const_cast<char*> (A.type.c_str());
	char* d3_size = reinterpret_cast<char*> (&s2);
	char* d3 = const_cast<char*> (A.brand.c_str());
	char* d4_size = reinterpret_cast<char*> (&s3);
	char* d4 = const_cast<char*> (A.color.c_str());

	for (int i = 0; i < n1; i++) data[i] = d1[i];
	for (int i = 0; i < n2_size; i++) data[i + n1] = d2_size[i];
	for (int i = 0; i < n2; i++) data[i + n1 + n2_size] = d2[i];
	for (int i = 0; i < n3_size; i++) data[i + n1 + n2_size + n2] = d3_size[i];
	for (int i = 0; i < n3; i++) data[i + n1 + n2_size + n2 + n3_size] = d3[i];
	for (int i = 0; i < n4_size; i++) data[i + n1 + n2_size + n2 + n3_size + n3] = d4_size[i];
	for (int i = 0; i < n4; i++) data[i + n1 + n2_size + n2 + n3_size + n3 + n4_size] = d4[i];
}
void PrintBinaryFile(char* data, int n)
{
	fstream f_out("data.dat", ios::app | ios::binary);
	f_out.write((char*)&n, sizeof(int));
	f_out.write(data, n);
	f_out.close();
}
int main()
{
	char* data;
	int n = 0;
	pen A[3] =
	{
	  {100, "ballpoint", "Parker", "blue"},
	  {50, "gel", "Pelikan", "black"},
	  {10, "capillary", "Lamy", "red"}
	};
	for (int i = 0; i < 3; i++)
	{
		GetSeria(data, n, A[i]);
		PrintBinaryFile(data, n);
		delete[] data;
	}
}
