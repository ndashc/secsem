#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>

using namespace std;

int Size();
void Get(int&, double*);
void Out(int, double*);
double Rand();
void Add(int&, double*&, double, int);
void Delete(int&, double*&, int);

int main()
{
	setlocale(0, "Russian");
	int n = Size(), k;

	double* a = new double[n];
	Get(n, a);
	cout << "Считанный массив:"; Out(n, a);
	cout << "(количество элементов: " << n << ")\n";

	double x;
	cout << "\n----> Для числа, введенного с клавиатуры <----";
	cout << "\nВведите любое число, которое нужно вставить:\n x = "; cin >> x;

	k = n + 1;
	Add(n, a, x, k);
	cout << "1) Добавить элемент в конец массива: "; Out(n, a);

	k = 1;
	Add(n, a, x, k);
	cout << "2) Добавить элемент в начало массива: "; Out(n, a);

	cout << "\nНа какое место добавить элемент?\nk = "; cin >> k;
	if (k < 1 || k > n + 1) {
		do
		{
			cout << "Ошибка! Число не может быть поствлено на " << k
				<< " место. Попробуйте снова:\nk = "; cin >> k;
		} while (k < 1 || k > n + 1);
	}
	Add(n, a, x, k);
	cout << "3) Добавить элемент на k-ую позицию в массиве: "; Out(n, a);

	cout << "\n----> Для числа, сгенерированного случайно <----";
	x = Rand();
	cout << "\nСлучайно сгененрированное число:\n random x = " << x << endl;

	k = n + 1;
	Add(n, a, x, k);
	cout << "1) Добавить элемент в конец массива: "; Out(n, a);

	k = 1;
	Add(n, a, x, k);
	cout << "2) Добавить элемент в начало массива: "; Out(n, a);

	cout << "\nНа какое место добавить элемент?\nk = "; cin >> k;
	if (k < 1 || k > n + 1) {
		do
		{
			cout << "Ошибка! Число не может быть поствлено на " << k
				<< " место. Попробуйте снова:\nk = "; cin >> k;
		} while (k < 1 || k > n + 1);
	}
	Add(n, a, x, k);
	cout << "3) Добавить элемент на k-ую позицию в массиве: "; Out(n, a);
	cout << endl;

	k = n;
	Delete(n, a, k);
	cout << "4) Удалить последний элемент массива: "; Out(n, a);

	k = 1;
	Delete(n, a, k);
	cout << "5) Удалить первый элемент массива: "; Out(n, a);

	cout << "\nКакой элемент удалить?\nk = "; cin >> k;
	if (k < 1 || k > n) {
		do
		{
			cout << "Ошибка! Число не может быть удалено с " << k
				<< " места. Попробуйте снова:\nk = "; cin >> k;
		} while (k < 1 || k > n);
	}
	Delete(n, a, k);
	cout << "6) Удалить k-ый элемент массива: "; Out(n, a);

}

int Size()
{
	double s;
	int n = 0;
	ifstream arr;
	arr.open("array.txt");
	if (!arr) cout << "Осторожно! Массив пустой.\n";

	while (arr >> s) n++;			//Извлечение из потока и запись в S

	arr.close();
	return (n);
}

void Get(int& n, double* a)
{
	ifstream arr;
	arr.open("array.txt");

	if (!arr) return;

	for (int i = 0; i < n; i++) {
		arr >> a[i];
	}
	arr.close();
}

void Out(int n, double* a)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}

	cout << endl;
}

double Rand()
{
	double x;
	srand(-10);
	x = (double)rand() / ((double)rand() + 0.1);
	return (x);
}

void Add(int& n, double*& a, double x, int k)
{
	if (k < 1 || k > n + 1) {
		do
		{
			cout << "Ошибка! Число не может быть поствлено на " << k
				<< " место. Попробуйте снова:\nk = "; cin >> k;
		} while (k < 1 || k > n + 1);
	}

	double* t = new double[n + 1];
	n++;
	for (int i = 0; i < k; i++) {
		t[i] = a[i];
	}

	t[k - 1] = x;

	for (int i = k; i < n; i++) {
		t[i] = a[i - 1];
	}

	delete[] a;
	a = t;

}

void Delete(int& n, double*& a, int k)
{
	double* t = new double[n + 1];
	n--;
	for (int i = 0; i < k - 1; i++) {
		t[i] = a[i];
	}

	for (int i = k; i < n + 1; i++) {
		t[i - 1] = a[i];
	}

	delete[] a;
	a = t;
}
