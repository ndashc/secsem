#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
struct book
{
    string Title;
    string Author;
    int Year;
    int Pages;
};
struct MyStack
{
    struct Node
    {
        book data;
        Node* prev;
    };
    Node* Top = NULL;
    int Count = 0;
    bool Push(book);
    bool Pop(book&);
    void Info();
};
bool MyStack::Push(book dt)
{
    if (!Top)
    {
        Top = new Node;
        Top->prev = NULL;
        Count = 1;
    }
    else
    {
        Node* temp;
        temp = new Node;
        temp->prev = Top;
        Top = temp;
        Count++;
    }
    Top->data = dt;
    return true;
}
bool MyStack::Pop(book& dt)
{
    if (!Top) return false;
    Node* temp = Top->prev;
    dt = Top->data;
    delete Top;
    Top = temp;
    Count--;
    return true;
}
void MyStack::Info()
{
    if (!Top) cout << "Stack is empty" << endl;
    else
    {
        cout << endl << "Stack info: " << endl;
        cout << "\tStack size = " << Count << endl;
        cout << "\tTop Title = " << Top->data.Title << endl;
        cout << "\tTop Author = " << Top->data.Author << endl;
        cout << "\tTop Year = " << Top->data.Year << endl;
        cout << "\tTop Pages = " << Top->data.Pages << endl;
    }
}
void print(MyStack& S, MyStack& V)
{
    book dt;
    while (S.Count)
    {
        S.Pop(dt);
        cout << "\tTitle = " << dt.Title << endl;
        cout << "\tAuthor = " << dt.Author << endl;
        cout << "\tYear = " << dt.Year << endl;
        cout << "\tPages = " << dt.Pages << endl << endl;
        V.Push(dt);
    }
    while (V.Count)
    {
        V.Pop(dt);
        S.Push(dt);
    }
}
int main()
{
    MyStack S;
    MyStack V;
    book dt;
    ifstream file("book.txt");
    string line;
    while (getline(file, line))
    {
        istringstream line_F(line);
        line_F >> dt.Title >> dt.Author >> dt.Year >> dt.Pages;
        S.Push(dt);
    }
    file.close();
    int m = 1;
    while (m)
    {
        S.Info();
        cout << "1. Add book to basket" << endl;
        cout << "2. Pull an item from the basket" << endl;
        cout << "3. Clear the basket" << endl;
        cout << "4. Show basket contents" << endl;
        cout << "0. Exit" << endl;
        cin >> m;
        switch (m)
        {
        case 1:
        {
            system("cls");
            cout << "Enter book specifications:" << endl;
            cout << "Title = "; cin >> dt.Title;
            cout << "Author = "; cin >> dt.Author;
            cout << "Year = "; cin >> dt.Year;
            cout << "Pages = "; cin >> dt.Pages;
            S.Push(dt);
            system("cls");
            break;
        }
        case 2:
        {
            system("cls");
            book dt_x;
            bool metka = false;
            cout << "Enter book characteristics:" << endl;
            cout << "Title = "; cin >> dt_x.Title;
            cout << "Author = "; cin >> dt_x.Author;
            cout << "Year = "; cin >> dt_x.Year;
            cout << "Pages = "; cin >> dt_x.Pages;
            while (S.Count)
            {
                S.Pop(dt);
                if (dt.Title != dt_x.Title || dt.Author != dt_x.Author || dt.Year != dt_x.Year || dt.Pages != dt_x.Pages)
                {
                    V.Push(dt);
                }
                else
                {
                    metka = true;
                    break;
                }
            }
            if (!metka) cout << "Book not found" << endl;
            while (V.Count)
            {
                V.Pop(dt);
                S.Push(dt);
            }
            system("pause");
            system("cls");
            break;
        }
        case 3:
        {
            system("cls");
            while (S.Count)
            {
                S.Pop(dt);
            }
            break;
        }
        case 4:
        {
            system("cls");
            print(S, V);
            system("pause");
            system("cls");
            break;
        }
        }
    }

    while (S.Count)
    {
        S.Pop(dt);
    }
}
